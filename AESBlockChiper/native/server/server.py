import socket
import select
import sys
import time
from encrypt import mainEncrypt
from decrypt import mainDecrypt

server_address = ('127.0.0.1', 5000)
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind(server_address)
# Maximum connected client
server_socket.listen(5)

input_socket = [server_socket]

def converttobit(data):
    data

try:
  while True:
    read_ready, write_ready, exception = select.select(input_socket, [], [])
    for sock in read_ready:
      if sock == server_socket:
        client_socket, client_address = server_socket.accept()
        input_socket.append(client_socket)
      else:
        data = sock.recv(1024).decode()
        host, port = sock.getpeername()
        start_time = time.time()
        data2, word= mainEncrypt(data)
        print(time.time() - start_time)
        start_time2 = time.time()
        data3 = mainDecrypt(data2,word)
        print(time.time() - start_time2)
        if str(data):
          sock.send(data3.encode())
        else:
          sock.close()
          input_socket.remove(sock)

except KeyboardInterrupt:
  server_socket.close()
  sys.exit