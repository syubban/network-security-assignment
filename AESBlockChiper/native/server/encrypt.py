from numpy.random import seed
from numpy.random import randint
from decrypt import mainDecrypt

sBox = (
    0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76,
    0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0,
    0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15,
    0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75,
    0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84,
    0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF,
    0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8,
    0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2,
    0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73,
    0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB,
    0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79,
    0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08,
    0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A,
    0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E,
    0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF,
    0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16,
)

mulby2 = (
    0x00,0x02,0x04,0x06,0x08,0x0a,0x0c,0x0e,0x10,0x12,0x14,0x16,0x18,0x1a,0x1c,0x1e,
    0x20,0x22,0x24,0x26,0x28,0x2a,0x2c,0x2e,0x30,0x32,0x34,0x36,0x38,0x3a,0x3c,0x3e,
    0x40,0x42,0x44,0x46,0x48,0x4a,0x4c,0x4e,0x50,0x52,0x54,0x56,0x58,0x5a,0x5c,0x5e,
    0x60,0x62,0x64,0x66,0x68,0x6a,0x6c,0x6e,0x70,0x72,0x74,0x76,0x78,0x7a,0x7c,0x7e,	
    0x80,0x82,0x84,0x86,0x88,0x8a,0x8c,0x8e,0x90,0x92,0x94,0x96,0x98,0x9a,0x9c,0x9e,
    0xa0,0xa2,0xa4,0xa6,0xa8,0xaa,0xac,0xae,0xb0,0xb2,0xb4,0xb6,0xb8,0xba,0xbc,0xbe,
    0xc0,0xc2,0xc4,0xc6,0xc8,0xca,0xcc,0xce,0xd0,0xd2,0xd4,0xd6,0xd8,0xda,0xdc,0xde,
    0xe0,0xe2,0xe4,0xe6,0xe8,0xea,0xec,0xee,0xf0,0xf2,0xf4,0xf6,0xf8,0xfa,0xfc,0xfe,
    0x1b,0x19,0x1f,0x1d,0x13,0x11,0x17,0x15,0x0b,0x09,0x0f,0x0d,0x03,0x01,0x07,0x05,
    0x3b,0x39,0x3f,0x3d,0x33,0x31,0x37,0x35,0x2b,0x29,0x2f,0x2d,0x23,0x21,0x27,0x25,
    0x5b,0x59,0x5f,0x5d,0x53,0x51,0x57,0x55,0x4b,0x49,0x4f,0x4d,0x43,0x41,0x47,0x45,
    0x7b,0x79,0x7f,0x7d,0x73,0x71,0x77,0x75,0x6b,0x69,0x6f,0x6d,0x63,0x61,0x67,0x65,
    0x9b,0x99,0x9f,0x9d,0x93,0x91,0x97,0x95,0x8b,0x89,0x8f,0x8d,0x83,0x81,0x87,0x85,
    0xbb,0xb9,0xbf,0xbd,0xb3,0xb1,0xb7,0xb5,0xab,0xa9,0xaf,0xad,0xa3,0xa1,0xa7,0xa5,
    0xdb,0xd9,0xdf,0xdd,0xd3,0xd1,0xd7,0xd5,0xcb,0xc9,0xcf,0xcd,0xc3,0xc1,0xc7,0xc5,
    0xfb,0xf9,0xff,0xfd,0xf3,0xf1,0xf7,0xf5,0xeb,0xe9,0xef,0xed,0xe3,0xe1,0xe7,0xe5
)

mulby3 = (
    0x00,0x03,0x06,0x05,0x0c,0x0f,0x0a,0x09,0x18,0x1b,0x1e,0x1d,0x14,0x17,0x12,0x11,
    0x30,0x33,0x36,0x35,0x3c,0x3f,0x3a,0x39,0x28,0x2b,0x2e,0x2d,0x24,0x27,0x22,0x21,
    0x60,0x63,0x66,0x65,0x6c,0x6f,0x6a,0x69,0x78,0x7b,0x7e,0x7d,0x74,0x77,0x72,0x71,
    0x50,0x53,0x56,0x55,0x5c,0x5f,0x5a,0x59,0x48,0x4b,0x4e,0x4d,0x44,0x47,0x42,0x41,
    0xc0,0xc3,0xc6,0xc5,0xcc,0xcf,0xca,0xc9,0xd8,0xdb,0xde,0xdd,0xd4,0xd7,0xd2,0xd1,
    0xf0,0xf3,0xf6,0xf5,0xfc,0xff,0xfa,0xf9,0xe8,0xeb,0xee,0xed,0xe4,0xe7,0xe2,0xe1,
    0xa0,0xa3,0xa6,0xa5,0xac,0xaf,0xaa,0xa9,0xb8,0xbb,0xbe,0xbd,0xb4,0xb7,0xb2,0xb1,
    0x90,0x93,0x96,0x95,0x9c,0x9f,0x9a,0x99,0x88,0x8b,0x8e,0x8d,0x84,0x87,0x82,0x81,	
    0x9b,0x98,0x9d,0x9e,0x97,0x94,0x91,0x92,0x83,0x80,0x85,0x86,0x8f,0x8c,0x89,0x8a,
    0xab,0xa8,0xad,0xae,0xa7,0xa4,0xa1,0xa2,0xb3,0xb0,0xb5,0xb6,0xbf,0xbc,0xb9,0xba,
    0xfb,0xf8,0xfd,0xfe,0xf7,0xf4,0xf1,0xf2,0xe3,0xe0,0xe5,0xe6,0xef,0xec,0xe9,0xea,	
    0xcb,0xc8,0xcd,0xce,0xc7,0xc4,0xc1,0xc2,0xd3,0xd0,0xd5,0xd6,0xdf,0xdc,0xd9,0xda,	
    0x5b,0x58,0x5d,0x5e,0x57,0x54,0x51,0x52,0x43,0x40,0x45,0x46,0x4f,0x4c,0x49,0x4a,
    0x6b,0x68,0x6d,0x6e,0x67,0x64,0x61,0x62,0x73,0x70,0x75,0x76,0x7f,0x7c,0x79,0x7a,	
    0x3b,0x38,0x3d,0x3e,0x37,0x34,0x31,0x32,0x23,0x20,0x25,0x26,0x2f,0x2c,0x29,0x2a,
    0x0b,0x08,0x0d,0x0e,0x07,0x04,0x01,0x02,0x13,0x10,0x15,0x16,0x1f,0x1c,0x19,0x1a
)

#initialize key
def initKey():
    seed(1)
    #generate random
    key = randint(0, 127, 16)
    # print(key)
    hexkey = [hex(x) for x in key]
    keyString =''
    for x in hexkey:
        keyString += x[2:]
    print("key : ", keyString)
    # print("key in hex : ", hexkey)
    return hexkey

#convert to blocks function
def convertToBlocks(data):
    newString = list(data)
    addZero = 0
    if (len(newString) % 16) != 0:
        addZero = 16 - len(newString) % 16
    for i in range(addZero):
        newString.append('a')
    # matrix = np.reshape(newString, (int(len(newString)/4),4))
    # print(newString)
    matrix = []
    iter = 0
    for i in range(int(len(newString)/4)):
        new = []
        for j in range(4):
            new.append(newString[iter])
            iter += 1
        matrix.append(new)
    rez = [[matrix[j][i] for j in range(len(matrix))] for i in range(len(matrix[0]))]
    print("data convert to block : ")
    for row in rez:
        print(row)
    # print(rez[0][0], type(rez[0][0]))
    # transposeMatrix = np.array(matrix).T
    # print(matrix)
    return newString

#XOR Key with Data
# def roundKeyOp (key, dataTransposed):
    cols = len(dataTransposed)
    rows = len(dataTransposed[0])
    iter = int((cols*rows) / 16)-1
    # print(key)
    hexkey = key
    iterate = 0
    matrix = []
    for i in range(int(len(hexkey)/4)):
        new = []
        for j in range(4):
            new.append(hexkey[iterate])
            iterate += 1
        matrix.append(new)
    rez = [[matrix[j][i] for j in range(len(matrix))] for i in range(len(matrix[0]))]
    print("key convert to block : ")
    for row in rez:
        print(row)
    newKeyAfterXOR = []
    for i in range(len(dataTransposed)):
        for j in range(len(dataTransposed[0])):
            newKeyAfterXOR.append(hex(int((hex(ord(dataTransposed[i][j]))),base=16) ^ int(matrix[i][j],base=16)))
            # print(hex(ord(dataTransposed[i][j])) , "XOR with" , matrix[i][j] , " = " , hex(int((hex(ord(dataTransposed[i][j]))),base=16) ^ int(matrix[i][j],base=16)))
    # print(newKeyAfterXOR)
    iterate2 = 0
    matrix2 = []
    for i in range(int(len(newKeyAfterXOR)/4)):
        new2 = []
        for j in range(4):
            new2.append(newKeyAfterXOR[iterate2])
            iterate2 += 1
        matrix2.append(new)

    rez2 = [[matrix2[j][i] for j in range(len(matrix2))] for i in range(len(matrix2[0]))]
    print("new key after XOR Operation : ")
    for row in rez2:
        print(row)
    return newKeyAfterXOR
    
#Expanding Key
def expandingKey(key):
    hexkey = key
    iterate = 0
    #insert to w
    word = []
    for i in range(int(len(hexkey)/4)):
        new = []
        for j in range(4):
            new.append(hexkey[iterate])
            iterate += 1
        word.append(new)
    rez = [[word[j][i] for j in range(len(word))] for i in range(len(word[0]))]
    print("key convert to block : ")
    for row in rez:
        print(row)
    nextword = 4
    rcon = []
    rcon.append([0x01,0x00,0x00,0x00])
    rcon.append([0x02,0x00,0x00,0x00])
    rcon.append([0x04,0x00,0x00,0x00])
    rcon.append([0x08,0x00,0x00,0x00])
    rcon.append([0x10,0x00,0x00,0x00])
    rcon.append([0x20,0x00,0x00,0x00])
    rcon.append([0x40,0x00,0x00,0x00])
    rcon.append([0x80,0x00,0x00,0x00])
    rcon.append([0x1B,0x00,0x00,0x00])
    rcon.append([0x36,0x00,0x00,0x00])
    temp = []
    for i in range(4):
        new = []
        for j in range(4):
            new.append(word[i][j])
        temp.append(new)
    # print(temp)
    wordNext = word[:]
    p=0
    # rest = []
    iteratee = 3
    for i in range(10):
        for l in range(4):
            iteratee+=1
            if l == 0:
                # flip word
                currWord = wordNext[nextword-1][:]
                first = currWord[0]
                for k in range(3):
                    currWord[k] = currWord[k+1]
                currWord[3] = first
                #convert sbox
                rotWord = []
                for k in range(4):
                    convertInt = int(currWord[k], base=16)
                    intValue = sBox[convertInt]
                    rotWord.append(intValue)

                #key-4 xor
                wordToXOR = temp[p]
                literallyNew = []
                for k in range(4):
                    literallyNew.append(int(wordToXOR[k],base=16) ^ rotWord[k])
                # print(temp)
                # print(literallyNew)
                #xor with rcon
                # rConst = []
                newElem = []
                for j in range(4):
                    newElem.append(hex(literallyNew[j] ^ rcon[i][j]))
                # print(newElem)
                # rest.append(newElem)
                temp.append(newElem)
                # print(temp)
                wordNext.append(newElem)
                # print(wordNext)
                # rConst.append(newElem)
            
            else:
                res = []
                for j in range(4):
                    # print(temp[iteratee-4][j],temp[iteratee-1][j])
                    res.append(hex(int(temp[iteratee-4][j],base=16) ^ int(temp[iteratee-1][j], base=16)))
                temp.append(res)
                # print(res)
                # rest.append(res)
                wordNext.append(res)
            p+=1
        nextword += 4
    # print(rest)
    print("")
    print("Expanded key : ")
    j=0
    #print word
    for i in temp:
        print ("word[",j,"] = ",i)
        j+=1
    return temp

#Subbytes data
def subBytes(data):
    # print(data[0])
    newData = []
    iter = 0
    for i in range(len(data)):
        convertInt = int(data[iter], base=16)
        intValue = sBox[convertInt]
        newData.append(hex(intValue))
        iter+=1
    iterate2 = 0
    showData = []
    for i in range(int(len(newData)/4)):
        new = []
        for j in range(4):
            new.append(newData[iterate2])
            iterate2 += 1
        showData.append(new)
    rez = [[showData[j][i] for j in range(len(showData))] for i in range(len(showData[0]))]
    print("After SubBytes:")
    for row in rez:
        print(row)
    return newData

def startRound(data,key):
    input = []
    iter = 0
    # print(data)
    # print(key)
    loop = len(data)/16
    for k in range(int(loop)):
        for i in range(4):
            for j in range(4):
                # print(int(data[iter],base=16), int(key[i][j], base=16))
                input.append(hex(int(data[iter][0],base=16) ^ int(key[i][j], base=16)))
                iter+=1
    iterate2 = 0
    showInput = []
    for i in range(int(len(input)/4)):
        new = []
        for j in range(4):
            new.append(input[iterate2])
            iterate2 += 1
        showInput.append(new)
    rez = [[showInput[j][i] for j in range(len(showInput))] for i in range(len(showInput[0]))]
    print("First Round Output:")
    for row in rez:
        print(row)
    return input

def toHEX(data):
    newData = []
    for i in range(len(data)):
        new = []
        for j in range(len(data[0])):
            convertHex = hex(ord(data[i][j]))
            new.append(convertHex)
        newData.append(new)
    # print(newData)
    iterate2 = 0
    showData = []
    for i in range(int(len(newData)/4)):
        new = []
        for j in range(4):
            new.append(newData[iterate2])
            iterate2 += 1
        showData.append(new)
    rez = [[showData[j][i] for j in range(len(showData))] for i in range(len(showData[0]))]
    print("Data Converted to HEX:")
    for row in rez:
        print(row)
    return newData

def shiftRows(data):
    newData = data[:]
    temp = data[:]
    newData[0] = temp[0]
    newData[1] = temp[5]
    newData[2] = temp[10]
    newData[3] = temp[15]
    newData[4] = temp[4]
    newData[5] = temp[9]
    newData[6] = temp[14]
    newData[7] = temp[3]
    newData[8] = temp[8]
    newData[9] = temp[13]
    newData[10] = temp[2]
    newData[11] = temp[7]
    newData[12] = temp[12]
    newData[13] = temp[1]
    newData[14] = temp[6]
    newData[15] = temp[11]
    iterate2 = 0
    showData = []
    for i in range(int(len(newData)/4)):
        new = []
        for j in range(4):
            new.append(newData[iterate2])
            iterate2 += 1
        showData.append(new)
    rez = [[showData[j][i] for j in range(len(showData))] for i in range(len(showData[0]))]
    print("After ShiftRows:")
    for row in rez:
        print(row)
    # print(data)
    # print(newData)
    return newData

def mixColumns(data):
    newData = []
    newData.append(mulby2[int(data[0],base=16)]^mulby3[int(data[1],base=16)]^int(data[2],base=16)^int(data[3],base=16))
    newData.append(int(data[0],base=16)^mulby2[int(data[1],base=16)]^mulby3[int(data[2],base=16)]^int(data[3],base=16))
    newData.append(int(data[0],base=16)^int(data[1],base=16)^mulby2[int(data[2],base=16)]^mulby3[int(data[3],base=16)])
    newData.append(mulby3[int(data[0],base=16)]^int(data[1],base=16)^int(data[2],base=16)^mulby2[int(data[3],base=16)])    
    newData.append(mulby2[int(data[4],base=16)]^mulby3[int(data[5],base=16)]^int(data[6],base=16)^int(data[7],base=16))
    newData.append(int(data[4],base=16)^mulby2[int(data[5],base=16)]^mulby3[int(data[6],base=16)]^int(data[7],base=16))
    newData.append(int(data[4],base=16)^int(data[5],base=16)^mulby2[int(data[6],base=16)]^mulby3[int(data[7],base=16)])
    newData.append(mulby3[int(data[4],base=16)]^int(data[5],base=16)^int(data[6],base=16)^mulby2[int(data[7],base=16)])
    newData.append(mulby2[int(data[8],base=16)]^mulby3[int(data[9],base=16)]^int(data[10],base=16)^int(data[11],base=16))
    newData.append(int(data[8],base=16)^mulby2[int(data[9],base=16)]^mulby3[int(data[10],base=16)]^int(data[11],base=16))
    newData.append(int(data[8],base=16)^int(data[9],base=16)^mulby2[int(data[10],base=16)]^mulby3[int(data[11],base=16)])
    newData.append(mulby3[int(data[8],base=16)]^int(data[9],base=16)^int(data[10],base=16)^mulby2[int(data[11],base=16)])
    newData.append(mulby2[int(data[12],base=16)]^mulby3[int(data[13],base=16)]^int(data[14],base=16)^int(data[15],base=16))
    newData.append(int(data[12],base=16)^mulby2[int(data[13],base=16)]^mulby3[int(data[14],base=16)]^int(data[15],base=16))
    newData.append(int(data[12],base=16)^int(data[13],base=16)^mulby2[int(data[14],base=16)]^mulby3[int(data[15],base=16)])
    newData.append(mulby3[int(data[12],base=16)]^int(data[13],base=16)^int(data[14],base=16)^mulby2[int(data[15],base=16)])
    iterate2 = 0
    showData = []
    for i in range(int(len(newData)/4)):
        new = []
        for j in range(4):
            new.append(hex(newData[iterate2]))
            iterate2 += 1
        showData.append(new)
    rez = [[showData[j][i] for j in range(len(showData))] for i in range(len(showData[0]))]
    print("After MixColumns:")
    for row in rez:
        print(row)
    return newData


def nextRound(data,key):
    # print(key)
    finalRound = []
    for i in range(len(data)):
        iterKey = 0
        inputData = data[i]
        for j in range(9):
            print("")
            print("Round ",j+1,"Block ",i+1," input :")
            # print(input round)
            iterate2 = 0
            showData = []
            for k in range(int(len(inputData)/4)):
                new = []
                for l in range(4):
                    # print(data[i][iterate2])
                    new.append(inputData[iterate2])
                    iterate2 += 1
                showData.append(new)
            rez = [[showData[l][k] for l in range(len(showData))] for k in range(len(showData[0]))]
            for row in rez:
                print(row)
            #subbytes
            print("")
            newsubBytes = subBytes(inputData)
            #mixrows
            print("")
            afterShiftRows = shiftRows(newsubBytes)
            print("")
            afterMixColumns = mixColumns(afterShiftRows)
            iter = 0
            newData = []
            for k in range(4):
                for l in range(4):
                    # print(afterMixColumns[iter], key[k+iterKey][l])
                    newData.append(hex(afterMixColumns[iter] ^ int(key[k+iterKey][l], base=16)))
                    iter+=1
            inputData = newData
            iterKey +=4
        finalRound.append(inputData)
    return finalRound

        
def splitData(data):
    splitters = int(len(data)/16)
    newData = []
    iter = 0
    for i in range(splitters):
        temp = []
        for j in range(16):
            temp.append(data[iter])
            iter+=1
        newData.append(temp)
    return newData

def finalRound(data,key):
    outputData = []
    for i in range(len(data)):
        iterKey = 0
        inputData = data[i]
        print("")
        print("Round ",10,"Block ",i+1," input :")
        # print(input round)
        iterate2 = 0
        showData = []
        for k in range(int(len(inputData)/4)):
            new = []
            for l in range(4):
                # print(data[i][iterate2])
                new.append(inputData[iterate2])
                iterate2 += 1
            showData.append(new)
        rez = [[showData[l][k] for l in range(len(showData))] for k in range(len(showData[0]))]
        for row in rez:
            print(row)
        #subbytes
        print("")
        newsubBytes = subBytes(data[i])
        #mixrows
        print("")
        afterShiftRows = shiftRows(newsubBytes)
        iter = 0
        newData = []
        for k in range(4):
            for l in range(4):
                newData.append(hex(int(afterShiftRows[iter],base=16) ^ int(key[k+iterKey][l], base=16)))
                iter+=1
        inputData = newData
        iterKey +=4
        iterate2 = 0
        showData = []
        for k in range(int(len(inputData)/4)):
            new = []
            for l in range(4):
                # print(data[i][iterate2])
                new.append(inputData[iterate2])
                iterate2 += 1
            showData.append(new)
        rez = [[showData[l][k] for l in range(len(showData))] for k in range(len(showData[0]))]
        print("")
        print("Output Block ",i+1," :")
        for row in rez:
            print(row)
        outputData.append(inputData)
    return outputData

# def convertEncrypt(data):
    temp = ""
    for i in range(len(data)):
        for j in range(len(data[0])):
            int_val = int(data[i][j], base=16)
            str_val = chr(int_val)
            temp+=str_val
    print(temp)

#main function
def mainEncrypt(data):
    data1 = data
    print("data : ", data1)
    print("")
    dataTransposed = convertToBlocks(data1)
    print("")
    toHexData = toHEX(dataTransposed)
    print("")
    key = initKey()
    print("")
    word = expandingKey(key)
    print("")
    zeroRound = startRound(toHexData,word)
    splitTheData = splitData(zeroRound)
    beforeEndRound = nextRound(splitTheData,word[4:])
    outputData = finalRound(beforeEndRound,word[40:])
    # outputFix = mainDecrypt(outputData,word)
    # print(outputFix)
    return (outputData,word)

# if __name__ == "__main__":
#     main("nanda aristiawan pamungkas")