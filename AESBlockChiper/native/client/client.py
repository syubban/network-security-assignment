import socket
import sys
import os
from Crypto import Random
import signal
import sys

# from library.aes_chiper import AESChiper

file_key_path = os.getcwd()[:-6] + '/key.txt'

server_address = ('127.0.0.1', 5000)
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect(server_address)
sys.stdout.write('>> ')

try:
  while True:
    message = str(input())
    client_socket.send(message.encode())
    print(client_socket.recv(1024).decode())
    sys.stdout.write('>> ')
    
except KeyboardInterrupt:
  client_socket.close()
  sys.exit(0)