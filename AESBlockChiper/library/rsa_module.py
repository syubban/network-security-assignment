from Crypto.PublicKey import RSA

class RsaModule:
  def __init__(self):
    self.empty = ""

  def generateRSAkey():
      key = RSA.generate(2048)
      private_key = key.export_key()
      file_ref = open("private_key.pem", "wb")
      file_ref.write(private_key)
      file_ref.close()

      public_key = key.publickey().export_key()
      file_ref = open("public_key.pem", "wb")
      file_ref.write(public_key)
      file_ref.close()

  