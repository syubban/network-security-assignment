from Crypto import Random
from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.PublicKey import RSA
import hashlib
import binascii
import base64
import os

class AESChiper(object):
  def __init__(self, rsakey):
    self.block_size = AES.block_size
    # self.key_path = os.getcwd()[:-6] + '/key.txt'
    self.rsakey = RSA.importKey(open(rsakey).read())
    self.chiper_rsa = PKCS1_OAEP.new(self.rsakey)

  # def getKey(self):
  #   with open(self.key_path, "rb+") as f:
  #     data = f.read()
  #     f.close()
  #     return data

  def ecrypt(self, plaintext):
    plaintext = self.pad(plaintext)
    iv = Random.new().read(self.block_size)
    # print("enc", iv)
    sessionkey = Random.new().read(16)
    encrypt_sessionkey = self.chiper_rsa.encrypt(sessionkey)
    cipher = AES.new(sessionkey, AES.MODE_CBC, iv)

    # print(f"Session key: {sessionkey} \n")
    # print(f"Encrypted session key: {encrypt_sessionkey} \n")
    # print(f"Chiper text {cipher.encrypt(plaintext.encode())} \n")
    # print(f"iv: {iv} \n")

    ciphertext = encrypt_sessionkey + iv + cipher.encrypt(plaintext.encode())
    # print(f"panjang utama {len(ciphertext)} \n")

    # print(f"Total: {ciphertext} \n")
    # return base64.b64encode(iv + chiper.encrypt(plaintext.encode()))
    # return binascii.hexlify(bytearray(iv + chiper.encrypt(plaintext.encode())))
    print(f"Encrypted message : {ciphertext}")
    return ciphertext

  # def test(self, ciphertext):
  #     # print(f"Hasil total : {ciphertext} \n")
  #     start_pos = 0
  #     # print(f"apa ini {self.rsakey.size_in_bytes()}")
  #     # print(f"panjang 1 {len(ciphertext)} \n")
  #     encrypted_sessionkey = ciphertext[start_pos:self.rsakey.size_in_bytes()]
  #     # print(f"panjang 2 {len(ciphertext)} \n")
  #     start_pos += self.rsakey.size_in_bytes()
  #     iv = ciphertext[start_pos:(start_pos + self.block_size)]
  #     start_pos += self.block_size
  #     result = ciphertext[start_pos:]

  #     print(f"Encrypted session key tes : {encrypted_sessionkey} \n")
  #     print(f"IV tes : {iv} \n")
  #     print(f"Chipertext tes : {result} \n")

  def decrypt(self, chipertext):
    start_pos = 0
    # Get session key
    encrypted_sessionkey = chipertext[start_pos:self.rsakey.size_in_bytes()]
    # Get iv from chipertext
    start_pos += self.rsakey.size_in_bytes()
    iv = chipertext[start_pos:(start_pos + self.block_size)]
    # print("dec", iv)
    # Get actual chipertext
    start_pos += self.block_size
    chipertext = chipertext[start_pos:]
    sessionkey = self.chiper_rsa.decrypt(encrypted_sessionkey)
    cipher = AES.new(sessionkey, AES.MODE_CBC, iv)
    plaintext = self.unpad(cipher.decrypt(chipertext).decode('utf-8'))

    return plaintext


  # Apabila data kurang ketika dibagi ke 1 block, maka kita isi kekurangannya
  def pad(self, message):
    return message + (self.block_size - (len(message) % self.block_size)) \
      * '0'

  def unpad(self, padded):
    pad_len = 0 
     
    for pad_char in padded[::-1]:         
        if pad_char == '0':             
            pad_len += 1
        else:
          break

    return padded[:-pad_len]


class RsaModule:
  def __init__(self):
    self.empty = ""

  def generateRSAkey(self):
      key = RSA.generate(2048)
      private_key = key.export_key()
      file_ref = open("private_key.pem", "wb")
      file_ref.write(private_key)
      file_ref.close()

      public_key = key.publickey().export_key()
      file_ref = open("public_key.pem", "wb")
      file_ref.write(public_key)
      file_ref.close()

# if __name__ == "__main__":
#   print("")
#   # rsamodul = RsaModule()
#   # rsamodul.generateRSAkey()
#   aesChiperEncrypt = AESChiper('public_key.pem')
#   result = aesChiperEncrypt.ecrypt("coba enkripsi kalimat ini dong!")
#   # print(f"Hasil encrypt {result} \n")
#   # aesChiperEncrypt.test(result)
#   aesChiperDecrypt = AESChiper('private_key.pem')
#   decrypted = aesChiperDecrypt.decrypt(result)
#   print(f"Hasil decrypt {decrypted} \n")