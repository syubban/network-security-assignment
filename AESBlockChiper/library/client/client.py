import socket
import sys
import os
from Crypto import Random
import signal
import sys
import time
from library.aes_chiper import RsaModule

from library.aes_chiper import AESChiper

# file_key_path = os.getcwd()[:-6] + '/key.txt'

server_address = ('127.0.0.1', 5000)
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect(server_address)
sys.stdout.write('>> ')

# def signal_handler(sig, frame):
#     print('You pressed Ctrl+C!')
#     sys.exit(0)

# signal.signal(signal.SIGINT, signal_handler)
# print('Press Ctrl+C')
# signal.pause()

# def writeToFile(key):
#   with open(file_key_path, "wb") as f:
#     f.seek(0)
#     f.write(key)
#     f.truncate()

try:
  # key = Random.new().read(16)
  rsaModule = RsaModule()
  rsaModule.generateRSAkey()

  while True:
    message = str(input())

    # writeToFile(key)
    aesBlockChiper = AESChiper('public_key.pem')

    start_time = time.time()
    chipertext = aesBlockChiper.ecrypt(message)
    print("--- Encrypt run time in %s seconds ---" % (time.time() - start_time))

    client_socket.send(chipertext)
    print(client_socket.recv(1024).decode())
    sys.stdout.write('>> ')
    
except KeyboardInterrupt:
  client_socket.close()
  sys.exit(0)

# if __name__ == '__main__':
#   rsaModule = RsaModule()
#   rsaModule.generateRSAkey()