import socket
import select
import sys
import datetime
import time

from library.aes_chiper import AESChiper

server_address = ('127.0.0.1', 5000)
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind(server_address)
# Maximum connected client
server_socket.listen(5)

input_socket = [server_socket]

try:
  while True:
    read_ready, write_ready, exception = select.select(input_socket, [], [])
    for sock in read_ready:
      if sock == server_socket:
        client_socket, client_address = server_socket.accept()
        input_socket.append(client_socket)
      else:
        payload = sock.recv(1024)

        start_time = time.time()
        aesChiper = AESChiper("../client/private_key.pem")
        plaintext = aesChiper.decrypt(payload)
        print("--- Decrypt run time in %s seconds ---" % (time.time() - start_time))

        # print(f"Key : {aesChiper.getKey()}")
        print(f"Chipertext : {payload}")
        print(f"Plaintext : {plaintext}")

        host, port = sock.getpeername()

        if str(payload):
          sock.send(f"Plaintext: {plaintext}".encode())
        else:
          sock.close()
          input_socket.remove(sock)
except KeyboardInterrupt:
  server_socket.close()
  sys.exit(0)
  
# if __name__ == "__main__":
#   aesChiper = AESChiper("../client/private_key.pem")