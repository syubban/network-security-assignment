# Network Security Assignment

### <strong>[Configuration]</strong> : 
#### Follow this step in order (if you already did it, you can only run step 3)

1. Enter the directory using cd
2. run : [ python3 -m venv venv ] --> (create virtual env)
3. activate virtual env
- (macos/linux) [. venv/bin/activate]
- (windows) [venv\Scripts\activate]
6. run : [ pip3 install -r requirement.txt ]
7. open terminal and add this to enable relative import or if you have this error ```ModuleNotFoundError: No module named 'library'```:

- <strong>[For linux / mac]</strong>  export PYTHONPATH="${PYTHONPATH}:/path/to/your/rootproject/"

  - eg : export PYTHONPATH="${PYTHONPATH}:/Users/syubbanfakhriya/Desktop/Repository/network-security-assignment/AESBlockChiper/"

- <strong>[For windows]</strong> set PYTHONPATH=%PYTHONPATH%;C:\path\to\your\rootproject\

### <strong>[How to run]</strong> :
#### This section is about how we structured our project and how to run

- AESBlockChiper
  - First assignments, we use native and library based aes ecryption
  - For library or native : first enter library directory then enter server for server code, and client for client code
  - Run server first by ```python3 server.py``` then client using ```python3 client.py```
  - Send the message from client (the one with >> in the terminal)